package com.facrecon.FacReconUtility;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
//import org.springframework.transaction.annotation.EnableTransactionManagement;



@SpringBootApplication


public class FacReconUtilityApplication extends SpringBootServletInitializer {
	
	 private static final Logger LOGGER = LogManager.getLogger(FacReconUtilityApplication.class);


	
	 @Override
	    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
	        return application.sources(FacReconUtilityApplication.class);
	    }

	    public static void main(String[] args) throws Exception {
	        SpringApplication.run(FacReconUtilityApplication.class, args);
	        
	        LOGGER.info("Info level log message");
	        LOGGER.debug("Debug level log message");
	        LOGGER.error("---------------Application Error-----------");
	    }

}
