import { Angular2Csv } from 'angular2-csv';
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, Response, Headers, URLSearchParams, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/timeout';

import { Router } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { PaginatorModule, Paginator, DataTable } from 'primeng/primeng';
import { ShowHideColumnDirective } from './show-hide-column.directive';

import { EditReconComponentNA } from '../edit-recon-na/edit-recon-na.component';
import { EditReconComponentAPAC } from '../edit-recon-apac/edit-recon-apac.component';
import { isNgTemplate } from '@angular/compiler';
import { NgForm } from '@angular/forms';
import { selectRequiredValidatorDirective } from './select-required-validator.directive';

export interface RecordType {
  value: string;
  viewValue: string;
}

export interface BranchName {
  value: string;
  viewValue: string;
}

export interface SelectItem {
  label: string;
  value: string;
}

declare const $;
declare var jQuery: any;

@Component({
  selector: 'app-report-comparision',
  templateUrl: './report-comparision.component.html',
  styleUrls: ['./report-comparision.component.css'],
  providers: [EditReconComponentNA, EditReconComponentAPAC]
})

export class ReportComparisionComponent implements OnInit {
  recondates: any[];
  loop: any[];
  optionsNA = {
    fieldSeparator: ',',
    quoteStrings: '"',
    decimalseparator: '.',
    showLabels: false,
    headers: ['Account Name', 'Policy No', 'Record Type', 'Underwriter', 'Branch',
      'SF Gross Premium', 'SF FAC Premium', 'SF FAC Commission', 'CAM - RI Gross Premium',
      'CAM - RI Treaty Premium', 'CAM - RI Fac Premium', 'CAM - RI Fac Commission',
      'MEL Gross Premium', 'MEL Treaty Premium', 'MEL FAC Premium', 'MEL FAC Commission',
      'URS FAC Premium', 'URS FAC Commission', 'Payable FAC Premium', 'Payable FAC Commission',
      'SF Vs CAM', 'CAM Vs MEL', 'MEL Vs URS', 'URS Vs URS Payable', 'Reviewed', 'Recon Date'],
    showTitle: false,
    title: 'NA Fac Recon',
    useBom: false,
    removeNewLines: true,

  };


  optionsAPAC = {
    fieldSeparator: ',',
    quoteStrings: '"',
    decimalseparator: '.',
    showLabels: false,
    headers: ['ID', 'Batch Register', 'Recon Result', 'Recon Date', 'Account Name', 'Policy No.', 'Record Type', 'Underwriter', 'Branch',
      'SF Gross Premium', 'SF FAC Premium', 'SF FAC Commission', 'CAM - RI Gross Premium',
      'CAM - RI Fac Premium', 'CAM - RI Fac Commission', 'PLB Gross Premium', 'PLB FAC Premium',
      'PLB FAC Commission', 'SF Vs CAM', 'SF Vs PLB', 'CAM Vs PLB', 'Reviewed', 'Reviewed New']
  }

  isValidFormSubmitted = false;

  natransactiondetails: any;
  natransactiondetails1: any;
  natransactiondetails2: any;
  natransactiondetails3: any;
  narecondetails1: any;
  narecondetails2: any;
  apactransactiondetails1: any;
  apactransactiondetails2: any;
  cols: any[];
  cols1: any[];
  columnOptions: SelectItem[];
  columnOptions1: SelectItem[];
  totalRecords = 0;
  step = 0;
  rowid: any;
  rowNA: any;
  rowNA1: any;
  rowNA2: any;
  rowNA3: any;
  rowNA4: any;
  rowNA5: any;
  rowNA6: any;
  rowNA7: any;
  rowNA8: any;
  rowNA9: any;
  rowNA10: any;
  rowNA11: any;
  rowNA12: any;
  rowNA13: any;
  rowNA14: any;
  rowNA15: any;
  rowNA16: any;
  rowAPAC: any;
  rowAPAC1: any;
  rowAPAC2: any;
  rowAPAC3: any;
  rowAPAC4: any;
  rowAPAC5: any;
  rowAPAC6: any;
  rowAPAC7: any;
  rowAPAC8: any;
  rowAPAC9: any;
  errors: any;
  data: any;
  narecondetails: any;
  apacrecondetails: any;
  serverResopnse: string;
  showSpinner: boolean;
  selectedreport: string;
  faconly: string;
  accountName: string;
  policynumber: string;
  selected_policy: string;
  selected_id: string;
  selectedValueNa: string;
  selectedValue_Plb: string;
  selectedValueGc: string;
  selectedValuePlb: string;
  recondateselectedvlue: string = "-1";
  reviewflagselectedvalue: string;
  selectedValueGWP: string;
  selectedValueFac: string;
  selectedValueCert: string;
  selectedTextArea: string;
  reviewed: boolean = false;
  sfvscam: boolean = false;
  camvsmel: boolean = false;
  melvsurs: boolean = false;
  ursvspayable: boolean = false;
  sfvsri: boolean = false;
  sfvsplb: boolean = false;
  camvsplb: boolean = false;
  closeResult: string;
  gcitdreconres: JSON;
  plbitdreconres: JSON;
  sftransactdetails: JSON;
  plbitdrecdata = new MatTableDataSource();
  gcitdrecdata = new MatTableDataSource();
  gcitdreconcolumns = ['id', 'account_name', 'policy_number', 'program', 'underwriter', 'branchname', 'sf_gross_premium', 'sf_fac_premium', 'sf_fac_commission',
    'ri_gross_premium', 'ri_treaty_premium', 'ri_fac_premium', 'ri_fac_commission', 'mel_gross_premium', 'mel_treaty_premium', 'mel_fac_premium',
    'mel_fac_commission', 'urs_fac_premium', 'urs_fac_commission', 'payable_fac_premium', 'payable_fac_commission', 'sf_vs_cam', 'cam_vs_mel', 'mel_vs_urs',
    'urs_vs_payable', 'reviewed', 'button', 'button1'];
  plbitdreconcolumns = ['id', 'account_name', 'policy_number', 'program', 'underwriter', 'branchname', 'sf_gross_premium', 'sf_fac_premium', 'sf_fac_commission',
    'ri_gross_premium', 'ri_fac_premium', 'ri_treaty_premium', 'ri_fac_commission', 'plb_gross_premium', 'plb_fac_premium', 'plb_treaty_premium', 'plb_fac_commission',
    'sf_vs_cam', 'sf_vs_plb', 'cam_vs_plb', 'reviewed', 'button'];
  records_gc: RecordType[] = [
    { value: 'Global Construction', viewValue: 'Global Construction' }
  ];
  records_plb: RecordType[] = [
    { value: 'Global Construction', viewValue: 'Global Construction' },
    { value: 'Global Offshore', viewValue: 'Global Offshore' },
    { value: 'International Onshore', viewValue: 'International Onshore' },
    { value: 'International Property', viewValue: 'International Property' }
  ];
  branch_gc: BranchName[] = [
    { value: 'Atlanta', viewValue: 'Atlanta' },
    { value: 'Boston', viewValue: 'Boston' },
    { value: 'Chicago', viewValue: 'Chicago' },
    { value: 'Dallas', viewValue: 'Dallas' },
    { value: 'Houston', viewValue: 'Houston' },
    { value: 'Los Angeles', viewValue: 'Los Angeles' },
    { value: 'Miami', viewValue: 'Miami' },
    { value: 'New York', viewValue: 'New York' },
    { value: 'Toronto', viewValue: 'Toronto' }
  ];
  branch_plb: BranchName[] = [
    { value: 'AU - Sydney', viewValue: 'AU - Sydney' },
    { value: 'Hong Kong', viewValue: 'Hong Kong' },
    { value: 'Manila', viewValue: 'Manila' },
    { value: 'MY - Kuala Lumpur', viewValue: 'MY - Kuala Lumpur' },
    { value: 'Shanghai', viewValue: 'Shanghai' },
    { value: 'Singapore', viewValue: 'Singapore' },
    { value: 'Tokyo', viewValue: 'Tokyo' }
  ];

  yesNo: string[] = ['Yes', 'No'];


  private sort: MatSort;
  private paginator: MatPaginator;

  @ViewChild(MatSort) set matSort(ms: MatSort) {

    this.sort = ms;

  }

  @ViewChild(MatPaginator) set matPaginator(mp: MatPaginator) {

    this.paginator = mp;

  }

  constructor(private editReconAPAC: EditReconComponentAPAC, private editReconNA: EditReconComponentNA, private http: Http, private modalService: NgbModal, private router: Router) { }


  ngOnInit(): void {

    this.cols = [
      { field: 'account_name', header: 'Account Name', width: '2%' },
      { field: 'policy_number', header: 'Policy Number', width: '1.25%' },
      { field: 'program', header: 'Record Type', width: '1.25%' },
      { field: 'branchname', header: 'Branch', width: '1%' },
      { field: 'underwriter', header: 'Underwriter', width: '1%' },
      { field: 'sf_gross_premium', header: 'Salesforce Gross Premium', width: '1.5%' },
      { field: 'sf_fac_premium', header: 'Salesforce FAC Premium', width: '1.5%' },
      { field: 'sf_fac_commission', header: 'Salesforce FAC Commission', width: '1.5%' },
      { field: 'ri_gross_premium', header: 'CAM RI Gross Premium', width: '1.5%' },
      { field: 'ri_treaty_premium', header: 'CAM RI Treaty Premium', width: '1.5%' },
      { field: 'ri_fac_premium', header: 'CAM RI FAC Premium', width: '1.5%' },
      { field: 'ri_fac_commission', header: 'CAM RI FAC Commission', width: '1.5%' },
      { field: 'mel_gross_premium', header: 'MEL Gross Premium', width: '1.5%' },
      { field: 'mel_treaty_premium', header: 'MEL Treaty Premium', width: '1.5%' },
      { field: 'mel_fac_premium', header: 'MEL FAC Premium', width: '1.5%' },
      { field: 'mel_fac_commission', header: 'MEL FAC Commission', width: '1.5%' },
      { field: 'urs_fac_premium', header: 'URS FAC Premium', width: '1.5%' },
      { field: 'urs_fac_commission', header: 'URS FAC Commission', width: '1.5%' },
      { field: 'payable_fac_premium', header: 'Payable FAC Premium', width: '1.5%' },
      { field: 'payable_fac_commission', header: 'Payable FAC Commission', width: '1.5%' },
      { field: 'sf_vs_cam', header: 'SF VS CAM', width: '2%' },
      { field: 'cam_vs_mel', header: 'CAM VS MEL', width: '2%' },
      { field: 'mel_vs_urs', header: 'MEL VS URS', width: '2%' },
      { field: 'urs_vs_payable', header: 'URS VS PAYABLE', width: '2%' }
    ];

    this.cols1 = [
      { field: 'account_name', header: 'Account Name' },
      { field: 'policy_number', header: 'Policy Number' },
      { field: 'program', header: 'Record Type' },
      { field: 'branchname', header: 'Branch' },
      { field: 'underwriter', header: 'Underwriter' },
      { field: 'sf_gross_premium', header: 'Salesforce Gross Premium' },
      { field: 'sf_fac_premium', header: 'Salesforce FAC Premium' },
      { field: 'sf_fac_commission', header: 'Salesforce FAC Commission' },
      { field: 'ri_gross_premium', header: 'CAM RI Gross Premium' },
      { field: 'ri_treaty_premium', header: 'CAM RI Treaty Premium' },
      { field: 'ri_fac_premium', header: 'CAM RI FAC Premium' },
      { field: 'ri_fac_commission', header: 'CAM RI FAC Commission' },
      { field: 'plb_gross_premium', header: 'PLB Gross Premium' },
      { field: 'plb_treaty_premium', header: 'PLB Treaty Premium' },
      { field: 'plb_fac_premium', header: 'PLB FAC Premium' },
      { field: 'plb_fac_commission', header: 'PLB FAC Commission' },
      { field: 'sf_vs_cam', header: 'SF VS CAM' },
      { field: 'sf_vs_cam', header: 'SF VS PLB' },
      { field: 'sf_vs_cam', header: 'CAM VS PLB' }
    ];

    this.columnOptions = [];
    this.columnOptions1 = [];
    for (let i = 0; i < this.cols.length; i++) {
      this.columnOptions.push({ label: this.cols[i].header, value: this.cols[i] });
    }

    for (let j = 0; j < this.cols1.length; j++) {
      this.columnOptions1.push({ label: this.cols1[j].header, value: this.cols1[j] });
    }
  }

  setDataSourceAttributes(block: string) {
    switch (block) {
      case 'narecon':
        this.gcitdrecdata.paginator = this.paginator;
        this.gcitdrecdata.sort = this.sort;
        break;
      case 'apacrecon':
        this.plbitdrecdata.paginator = this.paginator;
        this.plbitdrecdata.sort = this.sort;
        break;

    }

  }
  search() {
    if (this.selectedreport === 'gcitdrec') {
      //  this.compareSFvsCam();
      this.compareGcItdRecReport();
    }
    else if (this.selectedreport === 'plbitdrec') {
      this.comparePlbItdRecReport();
    }
  }

  compareGcItdRecReport() {
    this.loop = null;
    this.gcitdrecdata.paginator = this.paginator;
    this.gcitdrecdata.sort = this.sort;
    this.showSpinner = true;

    this.errors = null;
    const urlSearchParams = new URLSearchParams();
    const headers = new Headers();
    urlSearchParams.append('policyNumber', this.policynumber);
    urlSearchParams.append('faconly', this.faconly);
    urlSearchParams.append('accountName', this.accountName);
    urlSearchParams.append('selectedValueNa', this.selectedValueNa);
    urlSearchParams.append('selectedValueGc', this.selectedValueGc);
    urlSearchParams.append('recondateselectedvlue', this.recondateselectedvlue);
    urlSearchParams.append('reviewflagselectedvalue', this.reviewflagselectedvalue);
    const options = new RequestOptions({ headers: headers });

    const apiUrl1 = '/api/extractreport/getNAReconRecord';
    //const apiUrl1 = '/FacReconService/extractreport/getNAReconRecord';

    this.http.post(apiUrl1, urlSearchParams, options).timeout(900000)
      .map(res => {
        this.serverResopnse = 'OK';

        this.gcitdreconres = res.json();
        this.loop = res.json();
        return res.json();

      })
      .catch(this.handleError)
      .subscribe(
        (data) => this.gcitdrecdata.data = data,
        (error) => this.errors = 'There are some problem with the server. Please try again after sometime.',
        () => this.showSpinner = (false));



    console.log(this.gcitdreconres);
  }

  comparePlbItdRecReport() {
    this.plbitdrecdata.paginator = this.paginator;
    this.plbitdrecdata.sort = this.sort;
    this.showSpinner = true;

    this.errors = null;
    const urlSearchParams = new URLSearchParams();
    const headers = new Headers();
    urlSearchParams.append('policyNumber', this.policynumber);
    urlSearchParams.append('faconly', this.faconly);
    urlSearchParams.append('accountName', this.accountName);
    urlSearchParams.append('selectedValue_Plb', this.selectedValue_Plb);
    urlSearchParams.append('selectedValuePlb', this.selectedValuePlb);
    urlSearchParams.append('recondateselectedvlue', this.recondateselectedvlue);
    urlSearchParams.append('reviewflagselectedvalue', this.reviewflagselectedvalue);
    const options = new RequestOptions({ headers: headers });

    const apiUrl1 = '/api/extractreport/getAPACReconRecord';
    //const apiUrl1 = '/FacReconService/extractreport/getAPACReconRecord';

    this.http.post(apiUrl1, urlSearchParams, options).timeout(900000)
      .map(res => {
        this.serverResopnse = 'OK';

        this.plbitdreconres = res.json();
        return res.json();

      })
      .catch(this.handleError)
      .subscribe(
        (data) => this.plbitdrecdata.data = data,
        (error) => this.errors = 'There are some problem with the server. Please try again after sometime.',
        () => this.showSpinner = (false));
  }

  handleError(error: Response) {
    console.error(error.status);

    return Observable.throw(error);
  }



  onSelectNa(selectedValueNa) {
    this.selectedValueNa = selectedValueNa;
  }
  onSelect_Plb(selectedValue_Plb) {
    this.selectedValue_Plb = selectedValue_Plb;
  }
  onSelectGc(selectedValueGc) {
    this.selectedValueGc = selectedValueGc;
  }
  onSelectPlb(selectedValuePlb) {
    this.selectedValuePlb = selectedValuePlb;
  }
  /*applyFilter(filterValue: string) {
         this.gcitdrecdata.filter = filterValue.trim().toLowerCase();
         this.plbitdrecdata.filter = filterValue.trim().toLowerCase();
       }*/
  setStep(index: number) {
    this.step = index;
  }

  navigate(routeaddress: string) {
    this.router.navigate([routeaddress]);
  }


  gcItdRecCsv(): Angular2Csv {

    for (let ooo of this.loop) {
      delete ooo['id'];
      delete ooo['batchRegister'];
      //delete ooo['participants'];
      delete ooo['reconResult'];
      delete ooo['melreinsurerlist'];
      delete ooo['ursreinsurerlist'];
      delete ooo['sfReportList'];
      delete ooo['camriTransactionReportList'];
      delete ooo['melTransactionReportList'];
      delete ooo['recon_date'];
      delete ooo['reviewedFlag'];

      delete ooo['sfRecordCount'];
      delete ooo['camRecordCount'];
      delete ooo['melRecordCount'];
      delete ooo['ursRecordCount'];
      delete ooo['locationCount'];
      delete ooo['payableRecordCount'];

    }

    return new Angular2Csv(this.loop, 'NA Fac Recon', this.optionsNA);
  }

  plbItdRecCsv(): Angular2Csv {

    const head = ['Account Name', 'Policy No.', 'Record Type', 'Underwriter', 'Branch',
      'SF Gross Premium', 'SF FAC Premium', 'SF FAC Commission', 'CAM - RI Gross Premium',
      'CAM - RI Fac Premium', 'CAM - RI Fac Commission', 'PLB Gross Premium', 'PLB FAC Premium',
      'PLB FAC Commission', 'SF Vs CAM', 'SF Vs PLB', 'CAM Vs PLB', 'Reviewed'];
    return new Angular2Csv(this.plbitdreconres, 'PLB ITD Recon', this.optionsAPAC);
  }



  openNA(rowNA, modalName: string) {
    this.rowid = rowNA.id;
    this.rowNA = rowNA.policy_number;
    this.rowNA1 = rowNA.account_name;
    this.rowNA2 = rowNA.branchname;
    this.rowNA3 = rowNA.underwriter;
    this.rowNA4 = rowNA.sf_gross_premium;
    this.rowNA5 = rowNA.sf_fac_premium;
    this.rowNA6 = rowNA.sf_fac_commission;
    this.rowNA7 = rowNA.ri_gross_premium;
    this.rowNA8 = rowNA.ri_fac_premium;
    this.rowNA9 = rowNA.ri_fac_commission;
    this.rowNA10 = rowNA.mel_gross_premium;
    this.rowNA11 = rowNA.mel_fac_premium;
    this.rowNA12 = rowNA.mel_fac_commission;
    this.rowNA13 = rowNA.urs_fac_premium;
    this.rowNA14 = rowNA.urs_fac_commission;
    this.rowNA15 = rowNA.payable_fac_premium;
    this.rowNA16 = rowNA.payable_fac_commission;
    if (rowNA.reconResult === null) {

      this.selectedValueGWP = null;
      this.selectedValueFac = null;
      this.selectedValueCert = null;
      this.selectedTextArea = null;

    }
    else {

      this.selectedValueGWP = rowNA.reconResult.gwp_matched;
      this.selectedValueFac = rowNA.reconResult.fac_matched;
      this.selectedValueCert = rowNA.reconResult.cert_available;
      this.selectedTextArea = rowNA.reconResult.comments;
    }

    if (modalName === 'narecondetails') {
      this.setParticipantMEL(rowNA.id);
      this.setParticipantURS(rowNA.id);
      jQuery('#narecondetails').modal('show');
    }
  }

  openAPAC(rowAPAC, modalName: string) {
    this.rowid = rowAPAC.id;
    this.rowAPAC = rowAPAC.policy_number;
    this.rowAPAC1 = rowAPAC.account_name;
    this.rowAPAC2 = rowAPAC.branchname;
    this.rowAPAC3 = rowAPAC.underwriter;
    this.rowAPAC4 = rowAPAC.sf_gross_premium;
    this.rowAPAC5 = rowAPAC.sf_fac_premium;
    this.rowAPAC6 = rowAPAC.ri_gross_premium;
    this.rowAPAC7 = rowAPAC.ri_fac_premium;
    this.rowAPAC8 = rowAPAC.plb_gross_premium;
    this.rowAPAC9 = rowAPAC.plb_fac_premium;
    if (rowAPAC.reconResult === null) {

      this.selectedValueGWP = null;
      this.selectedValueFac = null;
      this.selectedValueCert = null;
      this.selectedTextArea = null;


    }
    else {

      this.selectedValueGWP = rowAPAC.reconResult.gwp_matched;
      this.selectedValueFac = rowAPAC.reconResult.fac_matched;
      this.selectedValueCert = rowAPAC.reconResult.cert_available;
      this.selectedTextArea = rowAPAC.reconResult.comments;
    }
    if (modalName === 'apacrecondetails') {
      jQuery('#apacrecondetails').modal('show');
    }

  }

  openTransactionNA(rowNA, modalName: string) {

    this.rowid = rowNA.id;
    this.rowNA = rowNA.policy_number;
    this.rowNA1 = rowNA.account_name;
    this.rowNA2 = rowNA.branchname;
    this.rowNA3 = rowNA.underwriter;
    this.rowNA4 = rowNA.sf_gross_premium;
    this.rowNA5 = rowNA.sf_fac_premium;
    this.rowNA6 = rowNA.sf_fac_commission;
    this.rowNA7 = rowNA.ri_gross_premium;
    this.rowNA8 = rowNA.ri_fac_premium;
    this.rowNA9 = rowNA.ri_fac_commission;
    this.rowNA10 = rowNA.mel_gross_premium;
    this.rowNA11 = rowNA.mel_fac_premium;
    this.rowNA12 = rowNA.mel_fac_commission;
    this.rowNA13 = rowNA.urs_fac_premium;
    this.rowNA14 = rowNA.urs_fac_commission;
    this.rowNA15 = rowNA.payable_fac_premium;
    this.rowNA16 = rowNA.payable_fac_commission;
    if (modalName === 'natransactiondetails') {
      this.setSFTransactionNA(rowNA.id);
      this.setCamRiTransactionNA(rowNA.id);
      this.setMelTransactionNA(rowNA.id);
      jQuery('#natransactiondetails').modal('show');
    }
  }

  openTransactionAPAC(rowAPAC, modalName: string) {

    this.rowAPAC = rowAPAC.policy_number;
    this.rowAPAC1 = rowAPAC.account_name;
    this.rowAPAC2 = rowAPAC.branchname;
    this.rowAPAC3 = rowAPAC.underwriter;
    this.rowAPAC4 = rowAPAC.sf_gross_premium;
    this.rowAPAC5 = rowAPAC.sf_fac_premium;
    this.rowAPAC6 = rowAPAC.ri_gross_premium;
    this.rowAPAC7 = rowAPAC.ri_fac_premium;
    this.rowAPAC8 = rowAPAC.plb_gross_premium;
    this.rowAPAC9 = rowAPAC.plb_fac_premium;
    if (modalName === 'apactransactiondetails') {
      this.setSFTransactionAPAC(rowAPAC.policy_number);
      this.setCamRiTransactionAPAC(rowAPAC.policy_number);
      // this.setMelTransactionNA(rowNA.policy_number);
      jQuery('#apactransactiondetails').modal('show');
    }

  }

  close(modalName: string) {

    if (modalName === 'narecondetails') {
      jQuery('#narecondetails').modal('hide');
    } else if (modalName === 'apacrecondetails') {
      jQuery('#apacrecondetails').modal('hide');
    } else if (modalName === 'natransactiondetails') {
      jQuery('#natransactiondetails').modal('hide');
    } else if (modalName === 'apactransactiondetails') {
      jQuery('#apactransactiondetails').modal('hide');
    }
  }

  setParticipantMEL(id: string) {
    this.showSpinner = true;
    this.selected_id = id;

    this.errors = null;
    this.data = null;
    const urlSearchParams = new URLSearchParams();
    const headers = new Headers();

    urlSearchParams.append('id', id);

    const options = new RequestOptions({ headers: headers, params: urlSearchParams });

    const apiUrl1 = '/api/extractreport/getMELReinsurerList';
    //const apiUrl1 = '/FacReconService/extractreport/getMELReinsurerList';

    this.http.get(apiUrl1, options)
      .map(res => {
        this.serverResopnse = 'OK';
        return res.json();

      })
      .catch(this.handleError)
      .subscribe(
        (data) => this.narecondetails1 = data,
        (error) => this.errors = 'There are some problem with the server. Please try again after sometime.',
        () => this.showSpinner = (false));

  }

  setParticipantURS(id: string) {
    this.showSpinner = true;
    this.selected_id = id;

    this.errors = null;
    this.data = null;
    const urlSearchParams = new URLSearchParams();
    const headers = new Headers();

    urlSearchParams.append('id', id);

    const options = new RequestOptions({ headers: headers, params: urlSearchParams });

    const apiUrl1 = '/api/extractreport/getURSReinsurerList';
    //const apiUrl1 = '/FacReconService/extractreport/getURSReinsurerList';

    this.http.get(apiUrl1, options)
      .map(res => {
        this.serverResopnse = 'OK';
        return res.json();

      })
      .catch(this.handleError)
      .subscribe(
        (data) => this.narecondetails2 = data,
        (error) => this.errors = 'There are some problem with the server. Please try again after sometime.',
        () => this.showSpinner = (false));

  }


  onSaveNA(id: string, policy_Number: string) {

    this.loop = null;
    this.showSpinner = true;
    this.selected_id = id;
    this.selected_policy = policy_Number;




    this.errors = null;
    this.data = null;
    const urlSearchParams = new URLSearchParams();
    const headers = new Headers();

    urlSearchParams.append('id', id);
    urlSearchParams.append('policy_Number', policy_Number);
    urlSearchParams.append('selectedValueGWP', this.selectedValueGWP);
    urlSearchParams.append('selectedValueFac', this.selectedValueFac);
    urlSearchParams.append('selectedValueCert', this.selectedValueCert);
    urlSearchParams.append('selectedTextArea', this.selectedTextArea);

    urlSearchParams.append('policynumber', this.policynumber);
    urlSearchParams.append('faconly', this.faconly);
    urlSearchParams.append('accountName', this.accountName);
    urlSearchParams.append('selectedValueNa', this.selectedValueNa);
    urlSearchParams.append('selectedValueGc', this.selectedValueGc);
    urlSearchParams.append('recondateselectedvlue', this.recondateselectedvlue);
    urlSearchParams.append('reviewflagselectedvalue', this.reviewflagselectedvalue);

    jQuery('#narecondetails').modal('hide');

    const options = new RequestOptions({ headers: headers });

    const apiUrl1 = '/api/extractreport/saveNaReconDetails';
    //const apiUrl1 = '/FacReconService/extractreport/saveNaReconDetails';

    this.http.post(apiUrl1, urlSearchParams, options).timeout(900000)
      .map(res => {
        this.serverResopnse = 'OK';
        this.gcitdreconres = res.json();
        this.loop = res.json();
        return res.json();

      })
      .catch(this.handleError)
      .subscribe(
        (data) => this.gcitdrecdata.data = data,
        (error) => this.errors = 'There are some problem with the server. Please try again after sometime.',
        () => this.showSpinner = (false));


    this.selectedValueGWP = null;
    this.selectedValueFac = null;
    this.selectedValueCert = null;
    this.selectedTextArea = null;

    this.faconly = null;
    this.accountName = null;
    this.selectedValueNa = null;
    this.selectedValueGc = null;
    this.policynumber = null;



  }

  onSaveAPAC(id: string, policy_Number: string) {

    this.showSpinner = true;
    this.selected_id = id;
    this.selected_policy = policy_Number;




    this.errors = null;
    this.data = null;
    const urlSearchParams = new URLSearchParams();
    const headers = new Headers();

    urlSearchParams.append('id', id);
    urlSearchParams.append('policy_Number', policy_Number);
    urlSearchParams.append('selectedValueGWP', this.selectedValueGWP);
    urlSearchParams.append('selectedValueFac', this.selectedValueFac);
    urlSearchParams.append('selectedValueCert', this.selectedValueCert);
    urlSearchParams.append('selectedTextArea', this.selectedTextArea);

    urlSearchParams.append('policynumber', this.policynumber);
    urlSearchParams.append('faconly', this.faconly);
    urlSearchParams.append('accountName', this.accountName);
    urlSearchParams.append('selectedValueNa', this.selectedValueNa);
    urlSearchParams.append('selectedValueGc', this.selectedValueGc);
    urlSearchParams.append('recondateselectedvlue', this.recondateselectedvlue);
    urlSearchParams.append('reviewflagselectedvalue', this.reviewflagselectedvalue);

    jQuery('#apacrecondetails').modal('hide');

    const options = new RequestOptions({ headers: headers });

    const apiUrl1 = '/api/extractreport/saveApacReconDetails';
    //const apiUrl1 = '/FacReconService/extractreport/saveApacReconDetails';

    this.http.post(apiUrl1, urlSearchParams, options).timeout(900000)
      .map(res => {
        this.serverResopnse = 'OK';
        this.gcitdreconres = res.json();
        return res.json();

      })
      .catch(this.handleError)
      .subscribe(
        (data) => this.plbitdrecdata.data = data,
        (error) => this.errors = 'There are some problem with the server. Please try again after sometime.',
        () => this.showSpinner = (false));


    this.selectedValueGWP = null;
    this.selectedValueFac = null;
    this.selectedValueCert = null;
    this.selectedTextArea = null;

    this.faconly = null;
    this.accountName = null;
    this.selectedValueNa = null;
    this.selectedValueGc = null;
    this.policynumber = null;

  }

  popuLateReconDate() {

    this.data = null;
    this.errors = null;

    const headers = new Headers();
    const urlSearchParams = new URLSearchParams();

    urlSearchParams.append('selectedreport', this.selectedreport);

    const options = new RequestOptions({ headers: headers, params: urlSearchParams });

    const apiUrl1 = '/api/extractreport/getRecondates';
    //const apiUrl1 = '/FacReconService/extractreport/getRecondates';

    this.http.get(apiUrl1, options).timeout(900000).
      subscribe(response => {
        this.recondates = response.json()

      })

    // var dropDownList = document.getElementById("mySelect") as HTMLSelectElement;
    // dropDownList.selectedIndex = 0;

  }


  setSFTransactionNA(id: string) {

    this.showSpinner = true;
    this.selected_id = id;

    this.errors = null;
    this.data = null;
    const urlSearchParams = new URLSearchParams();
    const headers = new Headers();

    urlSearchParams.append('id', id);

    const options = new RequestOptions({ headers: headers, params: urlSearchParams });

    const apiUrl1 = '/api/extractreport/getSFTransactionDetails';
    //const apiUrl1 = '/FacReconService/extractreport/getSFTransactionDetails';

    this.http.get(apiUrl1, options).timeout(900000).
      map(res => {
        this.serverResopnse = 'OK';
        return res.json();


      })
      .catch(this.handleError)
      .subscribe(
        (data) => this.natransactiondetails1 = data,
        (error) => this.errors = 'There are some problem with the server. Please try again after sometime.',
        () => this.showSpinner = (false));

  }

  setCamRiTransactionNA(id: string) {

    this.showSpinner = true;
    this.selected_id = id;


    this.errors = null;
    this.data = null;
    const urlSearchParams = new URLSearchParams();
    const headers = new Headers();

    urlSearchParams.append('id', id);

    const options = new RequestOptions({ headers: headers, params: urlSearchParams });

    const apiUrl1 = '/api/extractreport/getCamRiTransactionDetails';
    //const apiUrl1 = '/FacReconService/extractreport/getCamRiTransactionDetails';

    this.http.get(apiUrl1, options).timeout(900000)
      .map(res => {
        this.serverResopnse = 'OK';
        return res.json();

      })
      .catch(this.handleError)
      .subscribe(
        (data) => this.natransactiondetails2 = data,
        (error) => this.errors = 'There are some problem with the server. Please try again after sometime.',
        () => this.showSpinner = (false));

  }

  setMelTransactionNA(id: string) {

    this.showSpinner = true;
    this.selected_id = id;

    this.errors = null;
    this.data = null;
    const urlSearchParams = new URLSearchParams();
    const headers = new Headers();

    urlSearchParams.append('id', id);

    const options = new RequestOptions({ headers: headers, params: urlSearchParams });

    const apiUrl1 = '/api/extractreport/getMelTransactionDetails';
    //const apiUrl1 = '/FacReconService/extractreport/getMelTransactionDetails';

    this.http.get(apiUrl1, options).timeout(900000)
      .map(res => {
        this.serverResopnse = 'OK';
        return res.json();

      })
      .catch(this.handleError)
      .subscribe(
        (data) => this.natransactiondetails3 = data,
        (error) => this.errors = 'There are some problem with the server. Please try again after sometime.',
        () => this.showSpinner = (false));

  }

  setSFTransactionAPAC(policyNumber: string) {

    this.showSpinner = true;
    this.selected_policy = policyNumber;

    this.errors = null;
    this.data = null;
    const urlSearchParams = new URLSearchParams();
    const headers = new Headers();

    urlSearchParams.append('policyNumber', policyNumber);

    const options = new RequestOptions({ headers: headers, params: urlSearchParams });

    const apiUrl1 = '/api/extractreport/getSFTransactionDetails';
    // const apiUrl1 = '/FacReconService/extractreport/getSFTransactionDetails';

    this.http.get(apiUrl1, options).timeout(900000).
      map(res => {
        this.serverResopnse = 'OK';
        return res.json();


      })
      .catch(this.handleError)
      .subscribe(
        (data) => this.apactransactiondetails1 = data,
        (error) => this.errors = 'There are some problem with the server. Please try again after sometime.',
        () => this.showSpinner = (false));

  }

  setCamRiTransactionAPAC(policy_Number: string) {

    this.showSpinner = true;


    this.errors = null;
    this.data = null;
    const urlSearchParams = new URLSearchParams();
    const headers = new Headers();

    urlSearchParams.append('policy_Number', policy_Number);

    const options = new RequestOptions({ headers: headers, params: urlSearchParams });

    const apiUrl1 = '/api/extractreport/getCamRiTransactionDetails';
    //const apiUrl1 = '/FacReconService/extractreport/getCamRiTransactionDetails';

    this.http.get(apiUrl1, options).timeout(900000)
      .map(res => {
        this.serverResopnse = 'OK';
        return res.json();

      })
      .catch(this.handleError)
      .subscribe(
        (data) => this.apactransactiondetails2 = data,
        (error) => this.errors = 'There are some problem with the server. Please try again after sometime.',
        () => this.showSpinner = (false));

  }

  // onFormSubmit(form: NgForm) {
  //   this.isValidFormSubmitted = false;
  //   if (form.touched && form.invalid) {
  //     return;
  //   }
  //   this.isValidFormSubmitted = true;
  //   form.resetForm();
  // }

}




